package com.cognizant.iot.retailmate.rmregistration.model;

/**
 * Created by 543898 on 2/24/17.
 */

public class RegistrationFieldModel {
    String textSource;
    Integer imageSource;

    public String getTextSource() {
        return textSource;
    }

    public void setTextSource(String textSource) {
        this.textSource = textSource;
    }

    public Integer getImageSource() {
        return imageSource;
    }

    public void setImageSource(Integer imageSource) {
        this.imageSource = imageSource;
    }
}
