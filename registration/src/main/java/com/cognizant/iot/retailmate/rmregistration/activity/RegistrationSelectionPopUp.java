package com.cognizant.iot.retailmate.rmregistration.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import com.cognizant.iot.retailmate.rmregistration.R;
import com.cognizant.iot.retailmate.rmregistration.adapter.RegistrationPopUpAdapter;
import com.cognizant.iot.retailmate.rmregistration.model.RegistrationFieldModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 543898 on 2/24/17.
 */

public class RegistrationSelectionPopUp extends AppCompatActivity {
    private RecyclerView mColorsRecyclerView;
    public static List<RegistrationFieldModel> popUpList;
    RegistrationPopUpAdapter registrationPopUpAdapter;
    public int flag;
    public Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_pop_up);
        popUpList = new ArrayList<>();
        flag = getIntent().getIntExtra("flag", -1);
        button = (Button) findViewById(R.id.registration_pop_up_button);
        mColorsRecyclerView = (RecyclerView) findViewById(R.id.recyclerview_pop_up_list);

        /*Adding respective data based on flag value from intent*/
        if (flag == 0)
            registrationPopUpAdapter = new RegistrationPopUpAdapter(RegistrationActivity.interestsList, 0);
        else if (flag == 1)
            registrationPopUpAdapter = new RegistrationPopUpAdapter(RegistrationActivity.colorsList, 1);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mColorsRecyclerView.setLayoutManager(mLayoutManager);
        mColorsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mColorsRecyclerView.setAdapter(registrationPopUpAdapter);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegistrationSelectionPopUp.this, RegistrationActivity.class);
                setResult(0, intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        //overriding on back pressed to do nothing
    }


}