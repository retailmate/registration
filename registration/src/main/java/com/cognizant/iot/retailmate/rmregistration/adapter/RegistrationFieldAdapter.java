package com.cognizant.iot.retailmate.rmregistration.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cognizant.iot.retailmate.rmregistration.R;
import com.cognizant.iot.retailmate.rmregistration.activity.RegistrationActivity;
import com.cognizant.iot.retailmate.rmregistration.activity.RegistrationSelectionPopUp;
import com.cognizant.iot.retailmate.rmregistration.model.RegistrationFieldModel;

import java.util.List;

/**
 * Created by 543898 on 2/24/17.
 */


public class RegistrationFieldAdapter extends RecyclerView.Adapter<RegistrationFieldAdapter.MyViewHolder> {

    private List<RegistrationFieldModel> interestsDataList;
    private List<RegistrationFieldModel> colorsDataList;
    private Context mContext;
    public static int INTEREST_CODE = 9988;
    public static int COLOR_CODE = 5284;


    int type;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;
        public ImageView imageView, deleteImageView;


        public MyViewHolder(View view) {
            super(view);
            textView = (TextView) view.findViewById(R.id.item_text);
            imageView = (ImageView) view.findViewById(R.id.item_image);
            deleteImageView = (ImageView) view.findViewById(R.id.delete_icon_image_view);

            if (type == 0 && getPosition() == RegistrationActivity.usersInterestsList.size() - 1) {
                deleteImageView.setVisibility(View.INVISIBLE);
            }
            if (type == 1 && getPosition() == RegistrationActivity.usersColorsList.size() - 1) {
                deleteImageView.setVisibility(View.INVISIBLE);
            }

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int viewPosition = getPosition();
                    if (type == 0) {
                        if (viewPosition == interestsDataList.size() - 1) {

                            Intent intent = new Intent(mContext, RegistrationSelectionPopUp.class);
                            intent.putExtra("flag", 0);
                            ((Activity) mContext).startActivityForResult(intent, INTEREST_CODE);

                        } else {
                            RegistrationActivity.usersInterestsList.remove(viewPosition);
                            notifyDataSetChanged();
                        }
                    } else {
                        if (viewPosition == colorsDataList.size() - 1) {
                            Intent intent = new Intent(mContext, RegistrationSelectionPopUp.class);
                            intent.putExtra("flag", 1);
                            ((Activity) mContext).startActivityForResult(intent, COLOR_CODE);

                        } else {
                            RegistrationActivity.usersColorsList.remove(viewPosition);
                            notifyDataSetChanged();
                        }
                    }
                }
            });


        }
    }


    public RegistrationFieldAdapter(List<RegistrationFieldModel> dataList, int i) {
        RegistrationFieldModel registrationFieldModel = new RegistrationFieldModel();
        registrationFieldModel.setTextSource("Add more");
        registrationFieldModel.setImageSource(R.drawable.ic_add);
        dataList.add(registrationFieldModel);
        type = i;
        if (i == 0) {
            this.interestsDataList = dataList;
        } else if (i == 1) {
            this.colorsDataList = dataList;
        }

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.interests_item_layout, parent, false);
        mContext = parent.getContext();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        RegistrationFieldModel registrationFieldModel = new RegistrationFieldModel();
        if (type == 0) {
            if (position == RegistrationActivity.usersInterestsList.size() - 1) {
                holder.deleteImageView.setVisibility(View.INVISIBLE);
            } else {
                holder.deleteImageView.setVisibility(View.VISIBLE);
            }
            registrationFieldModel = interestsDataList.get(position);
        } else if (type == 1) {
            if (position == RegistrationActivity.usersColorsList.size() - 1) {
                holder.deleteImageView.setVisibility(View.INVISIBLE);
            } else {
                holder.deleteImageView.setVisibility(View.VISIBLE);
            }
            registrationFieldModel = colorsDataList.get(position);
        }

        holder.textView.setText(registrationFieldModel.getTextSource());
        holder.imageView.setImageResource(registrationFieldModel.getImageSource());

    }

    @Override
    public int getItemCount() {
        if (type == 0) {
            return interestsDataList.size();
        } else {
            return colorsDataList.size();
        }
    }
}
