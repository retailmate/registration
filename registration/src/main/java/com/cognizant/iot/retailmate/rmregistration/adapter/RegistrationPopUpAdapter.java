package com.cognizant.iot.retailmate.rmregistration.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.cognizant.iot.retailmate.rmregistration.R;
import com.cognizant.iot.retailmate.rmregistration.activity.RegistrationActivity;
import com.cognizant.iot.retailmate.rmregistration.model.RegistrationFieldModel;

import java.util.List;

/**
 * Created by 543898 on 2/24/17.
 */

public class RegistrationPopUpAdapter extends RecyclerView.Adapter<RegistrationPopUpAdapter.MyViewHolder> {

    private List<RegistrationFieldModel> popUpList;
    Context mContext;
    int flag;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CheckBox popUpListText;

        public MyViewHolder(View view) {
            super(view);
            popUpListText = (CheckBox) view.findViewById(R.id.pop_up_list_text_view);


            popUpListText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean checked = ((CheckBox) v).isChecked();
                    if (flag == 0) {
                        if (checked) {
                            if (!RegistrationActivity.usersInterestsList.contains(popUpList.get(getPosition())))
                                RegistrationActivity.usersInterestsList.add(RegistrationActivity.usersInterestsList.size() - 1, popUpList.get(getPosition()));
                        } else {
                            RegistrationActivity.usersInterestsList.remove(popUpList.get(getPosition()));
                        }
                    } else if (flag == 1) {
                        if (checked) {
                            if (!RegistrationActivity.usersColorsList.contains(popUpList.get(getPosition())))
                                RegistrationActivity.usersColorsList.add(RegistrationActivity.usersColorsList.size() - 1, popUpList.get(getPosition()));
                        } else {
                            RegistrationActivity.usersColorsList.remove(popUpList.get(getPosition()));
                        }
                    }
                }
            });


        }
    }


    public RegistrationPopUpAdapter(List<RegistrationFieldModel> popUpList, int i) {
        this.popUpList = popUpList;
        this.flag = i;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pop_up_item_layout, parent, false);
        mContext = parent.getContext();

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        RegistrationFieldModel popUp = popUpList.get(position);
        holder.popUpListText.setText(popUp.getTextSource());
        if (flag == 0) {
            if (RegistrationActivity.usersInterestsList.contains(popUpList.get(position))) {
                holder.popUpListText.setChecked(true);
            }
        } else if (flag == 1) {
            if (RegistrationActivity.usersColorsList.contains(popUpList.get(position))) {
                holder.popUpListText.setChecked(true);
            }
        }
    }

    @Override
    public int getItemCount() {
        return popUpList.size();
    }
}

