package com.cognizant.iot.retailmate.rmregistration.textValidation;

import android.text.TextUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by 543898 on 2/28/17.
 */

public class TextValidationUtil {

    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public final static boolean isOnlyText(String target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return Pattern.compile("^[a-zA-Z ]+$").matcher(target).matches();
        }
    }

    public final static boolean isLength(String target, Integer size) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return (target.length() == size);
        }
    }

    public final static boolean isAtleastLength(String target, Integer size) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return (target.length() >= size);
        }
    }

    public final static boolean isOnlyNumber(String target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return Pattern.compile("^[0-9 ]+$").matcher(target).matches();
        }
    }
}
