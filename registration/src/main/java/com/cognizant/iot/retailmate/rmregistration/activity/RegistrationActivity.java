package com.cognizant.iot.retailmate.rmregistration.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.iot.retailmate.rmregistration.R;
import com.cognizant.iot.retailmate.rmregistration.adapter.RegistrationFieldAdapter;
import com.cognizant.iot.retailmate.rmregistration.model.RegistrationFieldModel;
import com.cognizant.iot.retailmate.rmregistration.textValidation.TextValidationUtil;
import com.cognizant.iot.retailmate.rmregistration.textValidation.TextValidator;

import java.util.ArrayList;
import java.util.List;

import static com.cognizant.iot.retailmate.rmregistration.adapter.RegistrationFieldAdapter.COLOR_CODE;
import static com.cognizant.iot.retailmate.rmregistration.adapter.RegistrationFieldAdapter.INTEREST_CODE;

public class RegistrationActivity extends AppCompatActivity {

    public Toolbar toolbar;

    public Button registerButton;

    public RecyclerView mInterestsRecyclerView, mColorsRecyclerView;

    public static List<RegistrationFieldModel> usersInterestsList = new ArrayList<>();
    public static List<RegistrationFieldModel> usersColorsList = new ArrayList<>();

    public static List<RegistrationFieldModel> interestsList = new ArrayList<>();
    public static List<RegistrationFieldModel> colorsList = new ArrayList<>();

    private RegistrationFieldAdapter mInterestsAdapter, mColorsAdapter;

    private EditText firstNameEditText, lastNameEditText, mobileNumberEditText, emailIdEditText, passwordEditText, reEnterPasswordEditText;


    private ScrollView scrollView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_layout);

        toolbar = (Toolbar) findViewById(R.id.toolbar_registration);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        scrollView = (ScrollView) findViewById(R.id.registration_scroll_view);

        firstNameEditText = (EditText) findViewById(R.id.first_name);
        lastNameEditText = (EditText) findViewById(R.id.last_name);
        mobileNumberEditText = (EditText) findViewById(R.id.mobile_no);
        emailIdEditText = (EditText) findViewById(R.id.email_id);
        passwordEditText = (EditText) findViewById(R.id.password);
        reEnterPasswordEditText = (EditText) findViewById(R.id.re_enter_password);

        registerButton = (Button) findViewById(R.id.register_button);

        /*Text change listeners for every field to do dynamic validation*/
        /*START*/

        firstNameEditText.addTextChangedListener(new TextValidator(firstNameEditText) {
            @Override
            public void validate(TextView textView, String text) {
                if (!(TextValidationUtil.isOnlyText(text.trim())))
                    firstNameEditText.setError(getString(R.string.valid_first_name_error));
                else if (!TextValidationUtil.isAtleastLength(text.trim(), 3))
                    firstNameEditText.setError(getString(R.string.first_name_atleast_3_characters_error));
                else
                    firstNameEditText.setError(null);
            }
        });


        lastNameEditText.addTextChangedListener(new TextValidator(lastNameEditText) {
            @Override
            public void validate(TextView textView, String text) {
                if (!(TextValidationUtil.isOnlyText(text.trim())))
                    lastNameEditText.setError(getString(R.string.valid_last_name));
                else if (!TextValidationUtil.isAtleastLength(text.trim(), 3))
                    lastNameEditText.setError(getString(R.string.last_name_atleast_3_characters_error));
                else
                    lastNameEditText.setError(null);
            }
        });

        mobileNumberEditText.addTextChangedListener(new TextValidator(mobileNumberEditText) {
            @Override
            public void validate(TextView textView, String text) {
                if (!(TextValidationUtil.isOnlyNumber(text.trim()) && (TextValidationUtil.isLength(text.trim(), 10))))
                    mobileNumberEditText.setError(getString(R.string.valid_mobile_number_error));
                else
                    mobileNumberEditText.setError(null);
            }
        });

        emailIdEditText.addTextChangedListener(new TextValidator(emailIdEditText) {
            @Override
            public void validate(TextView textView, String text) {
                if (!(TextValidationUtil.isValidEmail(text.trim())))
                    emailIdEditText.setError(getString(R.string.valid_email_id_error));
                else
                    emailIdEditText.setError(null);

            }
        });

        passwordEditText.addTextChangedListener(new TextValidator(passwordEditText) {
            @Override
            public void validate(TextView textView, String text) {
                if (!TextValidationUtil.isAtleastLength(text.trim(), 6))
                    passwordEditText.setError(getString(R.string.password_atleast_6_characters_error));
                else if (!reEnterPasswordEditText.getText().toString().equals(passwordEditText.getText().toString()))
                    reEnterPasswordEditText.setError(getString(R.string.passwords_do_not_match_error));
                else if (reEnterPasswordEditText.getText().toString().equals(passwordEditText.getText().toString()))
                    reEnterPasswordEditText.setError(null);
                else
                    passwordEditText.setError(null);

            }
        });

        reEnterPasswordEditText.addTextChangedListener(new TextValidator(reEnterPasswordEditText) {
            @Override
            public void validate(TextView textView, String text) {
                if (!passwordEditText.getText().toString().trim().equals(reEnterPasswordEditText.getText().toString()))
                    reEnterPasswordEditText.setError(getString(R.string.passwords_do_not_match_error));
                else
                    reEnterPasswordEditText.setError(null);

            }
        });
         /*END*/
        /*Text change listeners for every field to do dynamic validation*/


        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAllField();
                if (firstNameEditText.getError() != null || lastNameEditText.getError() != null || mobileNumberEditText.getError() != null || emailIdEditText.getError() != null || passwordEditText.getError() != null || reEnterPasswordEditText.getError() != null) {
                    scrollView.smoothScrollTo(0, 0);
                } else {
                    /*Start Next Activity*/
                    Toast.makeText(RegistrationActivity.this, "Proceed to register", Toast.LENGTH_LONG).show();

                }
            }
        });


        /*SET DATA FOR THE INTEREST ANS COLOR*/
        /*do API call here to populate required data*/
        setColorsList();
        setInterestsList();


        /*RECYCLER VIEWS FOR INTERESTS AND COLORS*/
        mInterestsRecyclerView = (RecyclerView) findViewById(R.id.interests_recycler_view);
        mInterestsAdapter = new RegistrationFieldAdapter(usersInterestsList, 0);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mInterestsRecyclerView.setLayoutManager(mLayoutManager);
        mInterestsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mInterestsRecyclerView.setAdapter(mInterestsAdapter);

        mColorsRecyclerView = (RecyclerView) findViewById(R.id.colors_recycler_view);
        mColorsAdapter = new RegistrationFieldAdapter(usersColorsList, 1);
        RecyclerView.LayoutManager mColorsLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mColorsRecyclerView.setLayoutManager(mColorsLayoutManager);
        mColorsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mColorsRecyclerView.setAdapter(mColorsAdapter);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == INTEREST_CODE)
            mInterestsAdapter.notifyDataSetChanged();
        if (requestCode == COLOR_CODE)
            mColorsAdapter.notifyDataSetChanged();
    }

    private boolean isEditTextEmpty(EditText editText) {
        if (editText.getText().toString().trim().equals(null) || editText.getText().toString().trim().equals("")) {
            editText.setError(getString(R.string.registration_field_empty));
            return false;
        } else {
            return true;
        }
    }

    private boolean checkAllField() {
        if (isEditTextEmpty(firstNameEditText) && isEditTextEmpty(lastNameEditText) && isEditTextEmpty(mobileNumberEditText) && isEditTextEmpty(emailIdEditText) && isEditTextEmpty(passwordEditText) && isEditTextEmpty(reEnterPasswordEditText)) {
            return true;
        } else {
            scrollView.smoothScrollTo(0, 0);
            return false;
        }
    }


    private void setColorsList() {
        RegistrationFieldModel registrationFieldModel1 = new RegistrationFieldModel();
        registrationFieldModel1.setTextSource("Blue");
        registrationFieldModel1.setImageSource(R.drawable.ic_color_blue);

        /*ADD ELEMENT TO USER LIST - TO SHOW THAT TWO ITEMS ARE ADDED*/
        colorsList.add(registrationFieldModel1);
        usersColorsList.add(registrationFieldModel1);

        RegistrationFieldModel registrationFieldModel2 = new RegistrationFieldModel();
        registrationFieldModel2.setTextSource("Red");
        registrationFieldModel2.setImageSource(R.drawable.ic_color_red);

        /*ADD ELEMENT TO USER LIST - TO SHOW THAT TWO ITEMS ARE ADDED*/
        colorsList.add(registrationFieldModel2);
        usersColorsList.add(registrationFieldModel2);

        RegistrationFieldModel registrationFieldModel3 = new RegistrationFieldModel();
        registrationFieldModel3.setTextSource("Yellow");
        registrationFieldModel3.setImageSource(R.drawable.ic_color_yellow);

        colorsList.add(registrationFieldModel3);

        RegistrationFieldModel registrationFieldModel4 = new RegistrationFieldModel();
        registrationFieldModel4.setTextSource("Green");
        registrationFieldModel4.setImageSource(R.drawable.ic_color_green);

        colorsList.add(registrationFieldModel4);

        RegistrationFieldModel registrationFieldModel5 = new RegistrationFieldModel();
        registrationFieldModel5.setTextSource("White");
        registrationFieldModel5.setImageSource(R.drawable.ic_color_white);

        colorsList.add(registrationFieldModel5);
    }

    private void setInterestsList() {
        RegistrationFieldModel registrationFieldModel1 = new RegistrationFieldModel();
        registrationFieldModel1.setTextSource("Fashion");
        registrationFieldModel1.setImageSource(R.drawable.ic_retailmate_logo);

        /*ADD ELEMENT TO USER LIST - TO SHOW THAT TWO ITEMS ARE ADDED*/
        interestsList.add(registrationFieldModel1);
        usersInterestsList.add(registrationFieldModel1);

        RegistrationFieldModel registrationFieldModel2 = new RegistrationFieldModel();
        registrationFieldModel2.setTextSource("Electronics");
        registrationFieldModel2.setImageSource(R.drawable.ic_retailmate_logo);

        /*ADD ELEMENT TO USER LIST - TO SHOW THAT TWO ITEMS ARE ADDED*/
        interestsList.add(registrationFieldModel2);
        usersInterestsList.add(registrationFieldModel2);

        RegistrationFieldModel registrationFieldModel3 = new RegistrationFieldModel();
        registrationFieldModel3.setTextSource("Home Decor");
        registrationFieldModel3.setImageSource(R.drawable.ic_retailmate_logo);

        interestsList.add(registrationFieldModel3);

        RegistrationFieldModel registrationFieldModel4 = new RegistrationFieldModel();
        registrationFieldModel4.setTextSource("Appliances");
        registrationFieldModel4.setImageSource(R.drawable.ic_retailmate_logo);

        interestsList.add(registrationFieldModel4);

        RegistrationFieldModel registrationFieldModel5 = new RegistrationFieldModel();
        registrationFieldModel5.setTextSource("Groceries");
        registrationFieldModel5.setImageSource(R.drawable.ic_retailmate_logo);

        interestsList.add(registrationFieldModel5);
    }

}
